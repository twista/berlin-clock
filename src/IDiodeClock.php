<?php
/**
 * Created by JetBrains PhpStorm.
 * User: twista
 * Date: 7.9.13
 * Time: 18:31
 * To change this template use File | Settings | File Templates.
 */

namespace Twista\codeKata;


interface IDiodeClock {
    public function getTimeFragment($index);
    public function setLights($index, $lights);
    public function showTime($time);
}