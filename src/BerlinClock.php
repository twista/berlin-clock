<?php

namespace Twista\codeKata;

/**
 * Class BerlinClock
 * @package Twista\codeKata
 */
class BerlinClock implements IObservable, IDiodeClock {

    /**
     * @var array
     */
    protected $timeFragments = array();

    /**
     * @var array
     */
    protected $clockRepresentation = array();

    /**
     * @param string $time
     * @return string
     * @throws IlegalArgumentException
     */
    public function showTime($time){
        if(!is_string($time))
            throw new IlegalArgumentException("Parametr 1 must be string, ".gettype($time)."given");

        if(!preg_match('/^[\d]{2}:[\d]{2}:[\d]{2}$/',$time))
            throw new IlegalArgumentException("Input string is not in valid format, hh:mm:ss");

        $this->timeFragments = explode(':',$time);
        $this->notify();


        return $this->formatClocks();
    }

    protected function formatClocks(){
        $delimiter = '<br>';
        return str_replace('|',$delimiter, $this->clockRepresentation[2] . $delimiter . $this->clockRepresentation[0] . $delimiter . $this->clockRepresentation[1]);
    }


    /**
     * @param $index
     * @return mixed
     * @throws MemberAccessException
     */
    public function getTimeFragment($index){
        if(!array_key_exists($index,$this->timeFragments))
            throw new MemberAccessException("Fragment with index $index doesn't exists.");
        return $this->timeFragments[$index];
    }

    /**
     * @param $index
     * @param $lights
     */
    public function setLights($index, $lights){
        $this->clockRepresentation[$index] = $lights;
    }

    /**
     * IObservable
     */

    /** @var array IObserver[] */
    protected $observers = array();

    /**
     * @param IObserver $observer
     */
    public function attach(IObserver $observer){
        $this->observers[] = $observer;
    }

    /**
     * @param IObserver $observer
     */
    public function detach(IObserver $observer){
        $this->observers = array_diff($this->observers, $observer);
    }

    /**
     * notify observers
     */
    public function notify(){
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

}