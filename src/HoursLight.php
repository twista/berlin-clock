<?php

namespace Twista\codeKata;

/**
 * Class HoursLight
 * @package Twista\codeKata
 */
class HoursLight implements IObserver {

    const INDEX = 0;

    /**
     * @param IObservable $observable
     */
    public function update(IObservable $observable) {
        if(!$observable instanceof IDiodeClock)
            return;

        $time = $this->resolveTime($observable->getTimeFragment(self::INDEX));
        $observable->setLights(self::INDEX, $time);
    }

    /**
     * @param $time
     * @return string
     */
    protected function resolveTime($time){
        $fiveMultiples = floor($time / 5);
        $firstRow = str_repeat('R',$fiveMultiples);
        $firstRow = str_pad($firstRow, 4, '0', STR_PAD_RIGHT);

        $oneMultiples = $time - ($fiveMultiples*5);
        $secondRow = str_repeat('R',$oneMultiples);
        $secondRow = str_pad($secondRow, 4, '0', STR_PAD_RIGHT);

        return sprintf("%s|%s", $firstRow, $secondRow);
    }

}