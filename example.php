<?php

require __DIR__ . '/src/autoload.php';

use Twista\codeKata as Twista;


$clock = new Twista\BerlinClock;

$clock->attach(new Twista\SecondsLight());
$clock->attach(new Twista\MinutesLight());
$clock->attach(new Twista\HoursLight());

$times = array(
    '00:00:00a',
    '13:17:01',
    '23:59:59',
    '24:00:00'
);

foreach ($times as $time) {
    echo ('<br><br>'.$time.'<br><br>');

    echo $clock->showTime($time);

}

